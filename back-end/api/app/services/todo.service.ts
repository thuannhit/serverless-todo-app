import AWS from "aws-sdk";
import { TaskCreation } from "../dto/taskCreation.dto";
const docClient = new AWS.DynamoDB.DocumentClient();
const tableName = "todo_tasks";
export class TodoService {
  //   private books: Model<any>;
  constructor() {
    // this.books = books;
  }

  /**
   * Create book
   * @param params
   */
  protected async createTask(newTask: TaskCreation): Promise<object> {
    try {
      const params = {
        TableName: tableName,
        Item: {
          title: newTask.name,
          assignee: newTask.assignee,
          due_date: newTask.due_date,
        },
      };
      const result = await docClient.put(params).promise();

      return result;
    } catch (err) {
      console.error(err);

      throw err;
    }
  }

  //   /**
  //    * Update a book by id
  //    * @param id
  //    * @param data
  //    */
  //   protected updateBooks(id: number, data: object) {
  //     return this.books.findOneAndUpdate({ id }, { $set: data }, { new: true });
  //   }

  /**
   * Find tasks list
   */
  protected findTasks() {
    const params = {
      TableName: tableName,
    };
    return docClient.scan(params).promise();
  }

  /**
   * Find task base on id
   * @param id
   */
  protected findOneById(id: number) {
    return docClient
      .get({ TableName: "todo_tasks", Key: { _id: id } })
      .promise();
  }

  //   /**
  //    * Delete book by id
  //    * @param id
  //    */
  //   protected deleteOneBookById(id: number) {
  //     return this.books.deleteOne({ id });
  //   }
}
