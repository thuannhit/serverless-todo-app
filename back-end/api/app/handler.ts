import { Handler, Context } from 'aws-lambda';
// import dotenv from 'dotenv';
// import path from 'path';
// const dotenvPath = path.join(__dirname, '../', `config/.env.${process.env.NODE_ENV}`);
// dotenv.config({
//   path: dotenvPath,
// });

// import { books } from './model';
import { TodoController } from './controllers/todo.controller';
const todoController = new TodoController();

export const create: Handler = (event: any, context: Context) => {
  return todoController.create(event, context);
};

// export const update: Handler = (event: any) => todoController.update(event);

export const find: Handler = () => todoController.find();

export const findOne: Handler = (event: any, context: Context) => {
  return todoController.findOne(event, context);
};

// export const deleteOne: Handler = (event: any) => todoController.deleteOne(event);