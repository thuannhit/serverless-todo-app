export class TaskCreation {
    name: string;
    description?: string;
    assignee: string;
    due_date: number;
  }