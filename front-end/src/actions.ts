import { BaseAction, actionIds } from "./common";

export const newTaskCreationReqStartAction = (inputData: any): BaseAction => {
  return {
    type: actionIds.CREATE_NEW_TASK_START,
    payload: inputData,
  }
};

export const todoTasksListReqStartAction = (): BaseAction => ({
  type: actionIds.GET_TODO_TASKS_LIST_START,
  payload: null,
});

export const todoTasksListReqCompletedAction = (taskList: any): BaseAction => ({
  type: actionIds.GET_TODO_TASKS_LIST_COMPLETED,
  payload: taskList,
});

export const newTaskCreationReqCompletedAction = (
  createdTask: any
  ): BaseAction => ({
    type: actionIds.NEW_TASK_CREATION_COMPLETED,
    payload: createdTask,
  });
  
  export const errorAPIAction = (inputData: any): BaseAction => {
    return {
      type: actionIds.ERROR_API,
      payload: inputData,
    }
  };