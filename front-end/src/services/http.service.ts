import axios, { AxiosResponse } from "axios";
import IHttp from "../interfaces/http.interface";
const baseUrl = 'http://thuan.nguyen.com'
const Http = (): IHttp => {
  return {
    get: async <T>(url: string): Promise<T> => {
      return axios.get(baseUrl+'/'+url).then((data) => data.data);
    },
    post: async <T>(url: string, data: any): Promise<T> => {
      return axios.post(url, data).then((res) => res.data);
    },
  };
};

export default Http