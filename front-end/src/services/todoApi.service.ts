import Http from "./http.service";
import { handleAPIError } from "./errorHandler.service";
const http = Http();

const toDoApiService = () => {
  return {
    createNewTask: (data: any) => {
      return http.post("tasks", data);
    },
    getTaskList: async (): Promise<any> => {
      return http.get("tasks");
    },
  };
};

export default toDoApiService;
