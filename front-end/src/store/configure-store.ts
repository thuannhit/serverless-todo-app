import { createStore, compose, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { rootReducers } from "../reducers";
import { rootSaga } from "../sagas";
import {errorAPIAction} from '../actions'
declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const configureStoreProd = (initialState: any) => {
  const sagaMiddleware = createSagaMiddleware({
    onError: (err) => {
      console.log('ERRROR NE THUAN')
      store.dispatch(errorAPIAction(err))
    }
  });
  const middlewares = [sagaMiddleware];

  let store = createStore(
    rootReducers,
    initialState,
    compose(applyMiddleware(...middlewares))
  );

  sagaMiddleware.run(rootSaga);

  return store;
};

const configureStoreDev = (initialState: any) => {
  const sagaMiddleware = createSagaMiddleware({
    onError: (err) => {
      console.log('ERRROR NE THUAN')
      store.dispatch(errorAPIAction(err))
    }
  });
  const middlewares = [sagaMiddleware];
  const composeEnhancer =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const store = createStore(
    rootReducers,
    initialState,
    composeEnhancer(applyMiddleware(...middlewares))
  );
  sagaMiddleware.run(rootSaga);
  return store;
};

export const configureStore =
  process.env.NODE_ENV !== "development"
    ? configureStoreProd
    : configureStoreDev;
