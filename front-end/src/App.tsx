import React, { FC, useState } from "react";
import "./App.css";
import { Layout, Menu, Breadcrumb } from "antd";
import {
  PieChartOutlined,
} from "@ant-design/icons";
import TaskCreationForm from "./components/task-creation-form/TaskCreationForm";
import TaskList from "./components/tasks-list/TaskList";

const { Header, Content, Footer, Sider } = Layout;

const App: FC = (props) => {
  const [collapsed, setCollapseState] = useState(false);

  const onCollapse = (newState: boolean) => {
    setCollapseState(newState);
  };

  return (
    <div className="App">
      <Layout style={{ minHeight: "100vh" }}>
        <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
          <div className="logo" />
          <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
            <Menu.Item key="1" icon={<PieChartOutlined />}>
              TODO Lists
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }} />
          <Content style={{ margin: "0 16px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              <Breadcrumb.Item>Tasks</Breadcrumb.Item>
            </Breadcrumb>
            <div
              className="site-layout-background"
              style={{ padding: 24, minHeight: 360 }}
            >
              Todo list
              <TaskCreationForm></TaskCreationForm>
              <TaskList></TaskList>
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}>Thuan Nguyen ©2020</Footer>
        </Layout>
      </Layout>
    </div>
  );
};

export default App;
