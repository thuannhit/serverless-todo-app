export const actionIds = {
  GET_TODO_TASKS_LIST_START: "[0] Request to get the todo tasks list.",
  GET_TODO_TASKS_LIST_COMPLETED: "[0] Request to get the todo tasks list.",
  CREATE_NEW_TASK_START: "[0] Request to create new task.",
  NEW_TASK_CREATION_COMPLETED: "[1] New task created",
  ERROR_API: "There is error from API",
};

export interface BaseAction {
  type: string;
  payload?: any;
}
