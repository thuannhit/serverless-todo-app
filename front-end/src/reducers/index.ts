import { combineReducers } from 'redux';
import {
  toDoTaskReducer
} from './todo-list.reducer';
import {IState} from './todo-list.reducer'
import {BaseAction} from '../common'

export const rootReducers = combineReducers({
    toDoTaskReducer
});
export type RootStateType = ReturnType<typeof  rootReducers>
//TODO Please consider this point carefully =>> how to use mapping Object here