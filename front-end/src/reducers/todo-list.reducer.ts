import { BaseAction, actionIds } from "../common";

interface IATask {
  task_name: string;
  _id: number;
  assignee: string;
}
export interface IState {
  tasks: IATask[];
  isLoading: boolean;
}
const initialState = {
  tasks: [],
  isLoading: false,
};

export const toDoTaskReducer = (
  state: IState = initialState,
  action: BaseAction
) => {
  const newState = Object.assign({}, state);

  switch (action.type) {
    case actionIds.GET_TODO_TASKS_LIST_START:
      newState.tasks = action.payload;
      newState.isLoading = true;
      break;
    case actionIds.GET_TODO_TASKS_LIST_COMPLETED:
      newState.tasks = action.payload;
      newState.isLoading = false;
      break;

    default:
      break;
  }

  return newState;
};
