import { Table, Tag, Space } from "antd";
import React, { FC, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { todoTasksListReqStartAction } from "../../actions";
import { RootStateType } from "../../reducers";

const displayText = (status: number) => {
  switch (status) {
    case 1:
      return "New";
    case 2:
      return "In Progress";
    case 3:
      return "Done";
  }
};
const columns = [
  {
    title: "Task Name",
    dataIndex: "task_name",
    key: "task_name",
    render: (text: string) => <a>{text}</a>,
  },
  {
    title: "Assignee",
    dataIndex: "assignee",
    key: "assignee",
  },
  {
    title: "Due date",
    dataIndex: "due_date",
    key: "due_date",
    render: (date: number) => <span>{moment(date).format("DD/MM/YYYY")}</span>,
  },
  {
    title: "status",
    key: "status",
    dataIndex: "status",
    render: (status: number) => {
      const color =
        status === 1
          ? "green"
          : status === 2
          ? "yellow"
          : status === 3
          ? "purple"
          : "red";
      return (
        <Tag color={color} key={status}>
          {displayText(status)}
        </Tag>
      );
    },
    //   <>
    //     {status.map((status) => {

    //       let color = tag.length > 5 ? "geekblue" : "green";
    //       if (status === "loser") {
    //         color = "volcano";
    //       }
    //       return (
    //         <Tag color={color} key={status}>
    //           {status.toUpperCase()}
    //         </Tag>
    //       );
    //     })}
    //   </>
  },
  {
    title: "Action",
    key: "action",
    render: (record: any) => (
      <Space size="middle">
        <a>Edit</a>
        <a>Delete</a>
      </Space>
    ),
  },
];

// const data = [
//   {
//     key: "1",
//     task_name: "Implement feature A",
//     due_date: 1609931074012,
//     assignee: "Nguyen Van A",
//     status: 3,
//   },
//   {
//     key: "2",
//     task_name: "Implement feature B",
//     due_date: 1609931074012,
//     assignee: "Nguyen Van B",
//     status: 2,
//   },
//   {
//     key: "3",
//     task_name: "Implement feature C",
//     due_date: 1609931074012,
//     assignee: "Nguyen Van C",
//     status: 1,
//   },
// ];

const TaskList: FC = () => {
  const dispatch = useDispatch();
  const data = useSelector(
    (state: RootStateType) => state.toDoTaskReducer.tasks
  );
  // const isLoading = useSelector(
  //   (state: RootStateType) => state.toDoTaskReducer.isLoading
  // );
  useEffect(() => {
    dispatch(todoTasksListReqStartAction());
  });
  return (
    <div>
      <Table columns={columns} dataSource={data} />
    </div>
  );
};

function mapStateToProps(state: any) {
  return {
    categories: state.categories,
  };
}

export default TaskList;
