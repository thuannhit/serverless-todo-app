import "./TaskCreationForm.css";
import React, { FC } from "react";
import { Form, Input, Button, Select, DatePicker } from "antd";
import { useDispatch } from "react-redux";
import { FormInstance } from "antd/lib/form";
import { newTaskCreationReqStartAction } from "../../actions";
const { Option } = Select;

const layout = {
  labelCol: { lg: { span: 8 },  },
  wrapperCol: {lg: {span: 12} },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};

const TaskCreationForm: FC = () => {
  const dispatch = useDispatch();

  const formRef = React.createRef<FormInstance>();

  const onFinish = (values: any) => {
    dispatch(newTaskCreationReqStartAction(values));
  };

  const onReset = () => {
    formRef.current!.resetFields();
  };

  const config = {
    rules: [
      {
        type: "object" as const,
        required: true,
        message: "Please select time!",
      },
    ],
  };
  const dateFormat = "DD/MM/YYYY";

  return (
    <div>
      <Form {...layout} ref={formRef} name="control-ref" onFinish={onFinish}>
        <Form.Item
          name="taskName"
          label="Task name"
          rules={[{ required: true }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="assignee"
          label="Assignee"
          rules={[{ required: true }]}
        >
          <Select
            placeholder="Select a option and change input text above"
            allowClear
          >
            <Option value="Nguyen Huu Thuan">Nguyen Huu Thuan</Option>
            <Option value="Nguyen Van A">Nguyen Van A</Option>
            <Option value="Nguyen Van B">Nguyen Van B</Option>
            <Option value="Nguyen Van C">Nguyen Van C</Option>
          </Select>
        </Form.Item>
        <Form.Item name="date-picker" label="Due date" {...config}>
          <DatePicker format={dateFormat} />
        </Form.Item>
        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
          <Button htmlType="button" onClick={onReset}>
            Reset
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default TaskCreationForm;
