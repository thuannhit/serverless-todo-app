import {AxiosResponse} from "axios"
interface IHttp {
    get<T>(url: string): Promise<T>;
    post<T>(url: string, data: any): Promise<T>;
    // put<T>(url: string): Promise<T>;
    // patch<T>(url: string): Promise<T>;
    // delete<T>(url: string): Promise<T>;
}

export default IHttp