import { all, call, put, takeEvery } from "redux-saga/effects";
import ToDoApiSvc from "../services/todoApi.service";
import {
  todoTasksListReqCompletedAction,
  newTaskCreationReqCompletedAction,
} from "../actions";
import { actionIds } from "../common";
import { handleAPIError } from "../services/errorHandler.service";
const loadTaskList = ToDoApiSvc().getTaskList;
const createNewTask = ToDoApiSvc().createNewTask;

export function* watchNewTaskCreationRequestStart() {
  yield all([
    takeEvery(actionIds.CREATE_NEW_TASK_START, onNewTaskCreationStart),
  ]);
}

function* onNewTaskCreationStart(inputData: any) {
  const generatedNumber = yield call(createNewTask, inputData);
  console.log("generatedTask", generatedNumber);
  yield put(newTaskCreationReqCompletedAction(generatedNumber));
}

export function* watchLoadTasksList() {
  yield all([
    takeEvery(actionIds.GET_TODO_TASKS_LIST_START, handleGetTaskList),
  ]);
}
function* handleGetTaskList() {
  try {
    const taskList = yield call(loadTaskList);
    yield put(todoTasksListReqCompletedAction(taskList));
    yield put({ type: "LOADING_STATUS", status: "complete" });
  } catch (e) {
    console.log(e);
    yield put({ type: "LOADING_STATUS", status: "error", e });
  }
}

export function* watchAPIError() {
  yield all([takeEvery(actionIds.ERROR_API, handleErrorAPI)]);
}
function* handleErrorAPI(oError: any) {
  yield call(handleAPIError, oError);
}
