import { all, fork } from 'redux-saga/effects';
import { watchLoadTasksList, watchNewTaskCreationRequestStart} from './todo-collection.sagas';

export const rootSaga = function* root() {
  yield all([fork(watchLoadTasksList), fork(watchNewTaskCreationRequestStart)]);
};