const { Todo } = require('../DAL/todo');
const cachedTodoInstance = new Todo({});

class TodoManager {
    static update(updatedTodo) {
        const todo = new Todo(updatedTodo);
        return todo.update();
    }

    static create(createdTodo) {
        const todo = new Todo(createdTodo);
        return todo.create();
    }

    static delete(id) {
        const todo = new Todo({id});
        return todo.delete();
    }

    static list() {
        // const todo = new Todo();
        // return todo.list();

        return cachedTodoInstance.list();
    }
}

module.exports = { TodoManager };
