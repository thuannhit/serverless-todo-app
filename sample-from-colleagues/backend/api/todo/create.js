const { TodoManager } = require('common');
const schema = require('../schema/create.json');
const { Validator } = require('jsonschema');

const create = async (event, context) => {
    const payload = JSON.parse(event.body);
    const validator = new Validator();
    const checkValidator = validator.validate(payload, schema); 
    
    if(checkValidator.errors.length > 0) {
        return {
            statusCode: 400,
            body: JSON.stringify(
                { "message": "payload is invalid" },
                null,
                2
            ),
        }
    }

    try {
        const result = await TodoManager.create(payload);
        return {
            statusCode: 201,
            headers: {
                "Access-Control-Allow-Headers" : "*",
                "Access-Control-Allow-Origin": "*",
            },
            body: JSON.stringify(
                result,
                null,
                2
            ),
        };
    } catch (error) {
        return {
            statusCode: 500,
            body: JSON.stringify(
                error,
                null,
                2
            ),
        };
    }
}

module.exports = { create };
  
